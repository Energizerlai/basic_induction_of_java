### sun jdk监控和故障处理工具
| 名称   | 说明                                                                                                      |
| ------ | --------------------------------------------------------------------------------------------------------- |
| jps    | jvm process status tool,显示指定系统内所有的hotspot虚拟机进程                                             |
| jstat  | jvm statistics monitoring tool,用于收集hotspot虚拟机各方面的运行数据                                      |
| jinfo  | configuration info for java，显示虚拟机配置信息                                                           |
| jmap   | memory map for java,生成虚拟机的内存转储快照（heapdump文件）                                              |
| jhat   | jvm heap dump browser，用于分析heapmap文件，它会建立一个http/html服务器  让用户可以在浏览器上查看分析结果 |
| jstack | stack trace for java ,显示虚拟机的线程快照                                                                |

### jps常用的选项(虚拟机进程状况工具)
| 属性 | 作用                                               |
| ---- | -------------------------------------------------- |
| -p   | 只输出LVMID，省略主类的名称                        |
| -m   | 输出虚拟机进程启动时传递给主类main（）函数的参数   |
| -l   | 输出主类的全名，如果进程执行的是jar包，输出jar路径 |
| -v   | 输出虚拟机进程启动时jvm参数                        |

- 可以列出正在运行的虚拟机进程，并显示虚拟机执行主类名称以及这些进程的本地虚拟机唯一ID。
- jsp命令格式 jsp [options] [hostid]
- jps可以通过RMI协议开启了RMI服务的远程虚拟机进程状态，hostid为RMI注册表中注册的主机名。
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/211206_d57496d3_1478371.png "屏幕截图.png")

### jstat：虚拟机统计信息监视工具
jstat是用于监视虚拟机各种运行状态信息的命令行工具。它可以显示本地或者远程虚拟机进程中的类装载、内存、垃圾回收、JIT编译等运行数据，

- jstat的命令格式  jstat [option vmid [interval [s|ms] [count]] ]
- 对于铭霖格式中的VMID和LVMID，如过是本地虚拟机进程，VMID和LVMID是一致的，如果是远程虚拟机，那VMID的格式应当是：[protocol:] [//] lvmid[@hostname[:port]/servername]
- 参数interval 和count分别表示查询的间隔和次数，如果省略这两个参数，说明只查询一次。

```
#每250毫秒查询一次进程2764垃圾收集情况
jstat -gc 2764 250 29

```

![-class   监视装载类、卸载类、总空间以及类装载所耗费的时间
-gc      监视java堆状况，包括eden区、两个survivor区、老年代、永久代等的容量、已用空间、GC时间合计信息](https://images.gitee.com/uploads/images/2018/1113/212107_d63b3cf9_1478371.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1113/212126_30a6882a_1478371.png "屏幕截图.png")


### jinfo：java配置信息工具
- jinfo的作用是实时的查看和调整虚拟机各项参数。使用jps命令的-v参数可以查看虚拟机启动时显示指定的参数列表，但如果想知道未被显式指定的参数的系统默认值，除了去找资料以外，就得使用jinfo的-flag选项

　　jinfo格式 jinfo [option] pid

　　jinfo在windows 平台仍有很大的限制

### jmap：java内存映像工具
- jmap命令用于生成堆转储快照。jmap的作用并不仅仅为了获取dump文件，它还可以查询finalize执行队列、java堆和永久代的详细信息。如空间使用率、当前用的是哪种收集器等。
- 和jinfo命令一样，jmap在windows下也受到比较大的限制。除了生成dump文件的-dump选项和用于查看每个类的实例、控件占用统计的-histo选项在所有操作系用都提供之外，其余选项只能在linux/solaris下使用。

jmap格式 jmap [option] vmid
| 选项           | 作用                                                                                                        |
| -------------- | ----------------------------------------------------------------------------------------------------------- |
| -dump          | 生成java堆转储快照。格式为： -dump:[live,]format=b,file=<filename>,其中live子参数说明是否只dump出存活的对象 |
| -finalizerinfo | 显示在F-Queue中等待Finalizer线程执行finalize方法的对象。只在Linux/Solaris平台下有效                         |
| -heap          | 显示java堆详细信息，如使用哪种收集器、参数配置、分代情况等，在Linux/Solaris平台下有效                       |
| -jisto         | 显示堆中对象统计信息，包含类、实例对象、合集容量                                                            |
| -permstat      | 以ClassLoader为统计口径显示永久代内存状态。只在Linux/Solaris平台下有效                                      |
| -F             | 当虚拟机进程对-dump选项没有相应时。可使用这个选项强制生成dump快照。只在Linux/Solaris平台下有效              |

### jhat：虚拟机堆转储快照分析工具

Sun JDK提供jhat与jmap搭配使用，来分析dump生成的堆快照。jhat内置了一个微型的HTTP/HTML服务器，生成dump文件的分析结果后，可以在浏览器中查看。

　　用法举例 jhat test1.bin  

　　test1.bin为生成的dump文件。

分析结果默认是以包围单位进行分组显示，分析内存泄漏问题主要会使用到其中的“Heap Histogram”与OQL标签的功能。前者可以找到内存中总容量最大的对象。后者是标准的对象查询语言，使用类似SQL的语法对内存中的对象进行查询统计。

### jstack：java堆栈跟踪工具
- jstack命令用于生成虚拟机当前时刻的线程快照。线程快照就是当前虚拟机内每一条线程正在执行的方法堆栈集合，生成线程快照的主要目的是定位线程出现长时间停顿的原因，如线程死锁、死循环、请求外部资源导致长时间等待等。
- jstack 格式 jstack [option] vmid

option选项的合法值和具体含义
| 选项 | 作用                                         |
| ---- | -------------------------------------------- |
| -F   | 当正常输出的请求不被响应时，强制输出线程堆栈 |
| -l   | 除堆栈外，显示关于锁的附加信息               |
| -m   | 如果调用到本地方法的话，可以显示c/c++的堆栈  |

### 监控参数的含义如下：
- S0C：s0（from）的大小（KB）
- S1C：s1（from）的大小（KB）
- S0U：s0（from）已使用的空间（KB）
- S1U：s1(from)已经使用的空间(KB)
- EC：eden区的大小(KB)
- EU：eden区已经使用的空间(KB)
- OC：老年代大小(KB)
- OU：老年代已经使用的空间(KB)
- MC：元空间的大小（Metaspace）
- MU：元空间已使用大小（KB）
- CCSC：压缩类空间大小（compressed class space）
- CCSU：压缩类空间已使用大小（KB）
- YGC：新生代gc次数
- YGCT：新生代gc耗时（秒）
- FGC：Full gc次数
- FGCT：Full gc耗时（秒）
- GCT：gc总耗时（秒）
- Loaded：表示载入了类的数量
- Unloaded：表示卸载类的数量
- Compiled：表示编译任务执行的次数
- Failed：表示编译失败的次数
- Total：线程总数
- Runnable：正在运行的线程数
- Sleeping：休眠的线程数
- Waiting：等待的线程数

### YGC和FGC发生的具体场景
- YGC ：对新生代堆进行gc。频率比较高，因为大部分对象的存活寿命较短，在新生代里被回收。性能耗费较小。
- FGC ：全堆范围的gc。默认堆空间使用到达80%(可调整)的时候会触发fgc。以我们生产环境为例，一般比较少会触发fgc，有时10天或一周左右会有一次。

### 什么时候执行YGC和FGC
- edn空间不足,执行 young gc
- old空间不足，perm空间不足，调用方法System.gc() ，ygc时的悲观策略, dump live的内存信息时(jmap –dump:live)，都会执行full gc


 **gc信息统计** 
```
[gzpflm@web1 ~]$ jstat -gcutil 131010
  S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT     GCT   
  1.50   0.00  20.72  88.73  94.96  92.65 159018  588.283     4    0.113  588

S0     第一个幸存者区的占当前容量百分比 
S1     第二个幸存者区的占当前容量百分比 
E      年轻代中Eden（伊甸园）已使用的占当前容量百分比 
O       old代已使用的占当前容量百分比 
M       元空间已使用的占当前容量百分比 
CCS     压缩类空间已使用占当前容量百分比 
YGC     young gc次数
YGCT    young gc总时间
FGC     full gc 次数
FGCT    full gc 总时间
GCT     gc的总时间
```
