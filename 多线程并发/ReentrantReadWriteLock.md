### ReentrantReadWriteLock（读写锁）
- 读写锁维护着一对锁，一个读锁和一个写锁。通过分离读锁和写锁，使得并发性比一般的排他锁有了较大的提升：在同一时间可以允许多个读线程同时访问，但是在写线程访问时，所有读线程和写线程都会被阻塞。
- 读写锁最多支持65535个递归写入锁和65535个递归读取锁。
- 锁降级：遵循获取写锁、获取读锁在释放写锁的次序 写锁能够降级成为读锁
- 读写锁ReentrantReadWriteLock实现接口ReadWriteLock，该接口维护了一对相关的锁，一个用于只读操作，另一个用于写入操作。只要没有 writer，读取锁可以由多个 reader 线程同时保持。也就是说，成功获取读锁的线程会看到写入锁之前版本所做的所有更新。



### 读写锁的主要特性：
- 支持公平和非公平的获取锁的方式；
- 支持可重入。读线程在获取了读锁后还可以获取读锁；写线程在获取了写锁之后既可以再次获取写锁又可以获取读锁；
- 读取锁和写入锁都支持锁获取期间的中断；
- 允许从写入锁降级为读取锁，其实现方式是：先获取写入锁，然后获取读取锁，最后释放写入锁。但是，从读取锁升级到写入锁是不允许的；
- Condition支持。仅写入锁提供了一个 Conditon 实现；读取锁不支持 Conditon ，readLock().newCondition() 会抛出 UnsupportedOperationException。 


### 
ReadWriteLock接口定义，分别返回两个锁，分别为读锁和写锁。
```
public interface ReadWriteLock {

    Lock readLock();

    Lock writeLock();
}
```
ReentrantReadWriteLock与ReentrantLock一样，其锁主体依然是Sync，它的读锁、写锁都是依靠Sync来实现的。所以ReentrantReadWriteLock实际上只有一个锁，只是在获取读取锁和写入锁的方式上不一样而已，它的读写锁其实就是两个类：ReadLock、writeLock，这两个类都是lock实现。



 **代码实例** 
```
package com.example.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁
 * @author qxw
 * @data 2018年8月3日下午2:54:13
 */
public class ReentratReadWriteLockTest {

	//一个线程像List里面添加10个消息，添加消息的代码用写锁同步，另外三个线程不停的去取List中的最新的一条消息，取的操作用读锁保护。
	public static void main(String[] args) {
		message n=new message();
		//读的线程
		for (int i = 0; i <3; i++) {
			new Thread(()->{
					while(true){
						String pre = "";
						String s=n.getMeaage();
						if(s==null){continue;}						
						
						 if(!s.equals(pre)){
							  pre = s;
							  System.out.println(Thread.currentThread().getName() + " get the last news : " + s);
						 }
						 if(Integer.parseInt(s) == 9){
							 break;
						 }
					}
			},"读线程 thread").start();
		}
		
			//写的线程		
			new Thread(()->{
					for (int i = 0; i < 10; i++) {
						try {
//							Thread.sleep(100);
						} catch (Exception e) {
							e.printStackTrace();
						}
						n.add(i+"");
					}
			},"写线程  ").start();
		
	}


static class message{
	
    private final List<String> newsList = new ArrayList<>();
    //初始化读写锁
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    //读锁
    private Lock readLock = lock.readLock();
    //写锁
    private Lock writeLock = lock.writeLock();
 
    //读的方法
    public String getMeaage(){
    	//加锁
    	readLock.lock();
    	try {
			if(newsList.size()==0){ return null;}
			return  newsList.get(newsList.size()-1);
		}finally {
			readLock.unlock();
		}
    }
    
    
    public void add(String s){
    	writeLock.lock();
    	try{
    		newsList.add(s);
    		System.out.println("写锁获取  写入 message:" + s);
    	}finally{
    		writeLock.unlock();
    	}
    }
}
}
```

### ReentantReadWriteLock的读写状态设计
- 首先对于同步状态，或者说同步资源来说，在AQS里面维护的其实就是一个int型的变量state，state的值可以表示获取到同步状态的线程的数量。因此重入锁可以简单的以重入则增加state的值的形式实现。

- 对于ReentantReadWriteLock因为要维护两个锁（读/写），但是同步状态state只有一个，所以ReentantReadWriteLock采用“按位切割”的方式，所谓“按位切割”就是将这个32位的int型state变量分为高16位和低16位来使用，高16位代表读状态，低16位代表写状态。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0803/152447_2b5d7dc2_1478371.png "屏幕截图.png")


### 写锁的获取与释放（WriteLock）
写锁是一个可重入的独占锁，使用AQS提供的独占式获取同步状态的策略。


```
//获取写锁
public void lock() {
    sync.acquire(1);
}

//AQS实现的独占式获取同步状态方法
public final void acquire(int arg) {
    if (!tryAcquire(arg) &&
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
        selfInterrupt();
}

//自定义重写的tryAcquire方法
protected final boolean tryAcquire(int acquires) {
    /*
     * Walkthrough:
     * 1. If read count nonzero or write count nonzero
     *    and owner is a different thread, fail.
     * 2. If count would saturate, fail. (This can only
     *    happen if count is already nonzero.)
     * 3. Otherwise, this thread is eligible for lock if
     *    it is either a reentrant acquire or
     *    queue policy allows it. If so, update state
     *    and set owner.
     */
    Thread current = Thread.currentThread();
    int c = getState();
    int w = exclusiveCount(c);    //取同步状态state的低16位，写同步状态
    if (c != 0) {
        // (Note: if c != 0 and w == 0 then shared count != 0)
        //存在读锁或当前线程不是已获取写锁的线程，返回false
        if (w == 0 || current != getExclusiveOwnerThread())
            return false;
        //判断同一线程获取写锁是否超过最大次数，支持可重入
        if (w + exclusiveCount(acquires) > MAX_COUNT)    //
            throw new Error("Maximum lock count exceeded");
        // Reentrant acquire
        setState(c + acquires);
        return true;
    }
    //此时c=0,读锁和写锁都没有被获取
    if (writerShouldBlock() ||
        !compareAndSetState(c, c + acquires))
        return false;
    setExclusiveOwnerThread(current);
    return true;
}
```

 **取写锁的步骤如下：** 
- 1）判断同步状态state是否为0。如果state!=0，说明已经有其他线程获取了读锁或写锁，执行2）；否则执行5）。
- 2）判断同步状态state的低16位（w）是否为0。如果w=0，说明其他线程获取了读锁，返回false；如果w!=0，说明其他线程获取了写锁，执行步3。
- 判断获取了写锁是否是当前线程，若不是返回false，否则执行4）；
- 判断当前线程获取写锁是否超过最大次数，若超过，抛异常，反之更新同步状态（此时当前线程以获取写锁，更新是线程安全的），返回true。
- 此时读锁或写锁都没有被获取，判断是否需要阻塞（公平和非公平方式实现不同），如果不需要阻塞，则CAS更新同步状态，若CAS成功则返回true，否则返回false。如果需要阻塞则返回false。

参考链接 https://www.cnblogs.com/zaizhoumo/p/7782941.html