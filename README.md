# Java基础归纳

Java 知识点学习+面试核心知识，收录网上大部分面试题整理加看书笔记整理而来,继续完善中。



#### 常用集合
1. [Map系：HashMap, LinkedHashMap, TreeMap, WeakHashMap, EnumMap](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E9%9B%86%E5%90%88/Map%E7%B3%BB%E5%88%97.md)
1. [List系：ArrayList, LinkedList, Vector, Stack;](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E9%9B%86%E5%90%88/List,.md)
1. [Set系：HashSet, LinkedHashSet, TreeSet;](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E9%9B%86%E5%90%88/set.md)
1. [工具类：Collections,Arrays](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E9%9B%86%E5%90%88/Collections,Arrays.md)
1. [哪些集合类是线程安全的](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E9%9B%86%E5%90%88/%E5%93%AA%E4%BA%9B%E9%9B%86%E5%90%88%E7%B1%BB%E6%98%AF%E7%BA%BF%E7%A8%8B%E5%AE%89%E5%85%A8%E7%9A%84.md)
1. [这几道Java集合框架面试题几乎必问](https://github.com/Snailclimb/Java-Guide/blob/master/Java相关/这几道Java集合框架面试题几乎必问.md)
1.  [Java 集合框架常见面试题总结](https://github.com/Snailclimb/Java-Guide/blob/master/Java相关/Java集合框架常见面试题总结.md)
1. [ArrayList 源码学习](https://github.com/Snailclimb/Java-Guide/blob/master/Java相关/ArrayList.md)  
1. [LinkedList 源码学习](https://github.com/Snailclimb/Java-Guide/blob/master/Java相关/LinkedList.md)[HashMap(JDK1.8)源码学习](https://github.com/Snailclimb/Java-Guide/blob/master/Java相关/HashMap.md)  



### 对象
1. [对象的创建和生命周期](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%AF%B9%E8%B1%A1/%E5%AF%B9%E8%B1%A1%E7%9A%84%E5%88%9B%E5%BB%BA%E5%92%8C%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F.md)
1. [Java中的初始化和清理及类的加载](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%AF%B9%E8%B1%A1/Java%E4%B8%AD%E7%9A%84%E5%88%9D%E5%A7%8B%E5%8C%96%E5%92%8C%E6%B8%85%E7%90%86%E5%8F%8A%E7%B1%BB%E7%9A%84%E5%8A%A0%E8%BD%BD.md)
1. [内部类：静态内部类和匿名内部类的使用和区别](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%AF%B9%E8%B1%A1/%E5%86%85%E9%83%A8%E7%B1%BB.md)


#### JVM相关

1. [JMM内存模型](https://blog.csdn.net/u010391342/article/details/65443617)
1. [虚拟机类加载机制](hhttps://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/%E8%99%9A%E6%8B%9F%E6%9C%BA%E7%B1%BB%E5%8A%A0%E8%BD%BD%E6%9C%BA%E5%88%B6.md) 
1. [内存分配与回收策略](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/%E5%86%85%E5%AD%98%E5%88%86%E9%85%8D%E4%B8%8E%E5%9B%9E%E6%94%B6%E7%AD%96%E7%95%A5.md)
1. Class文件结构
1. [java运行时内存划分](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/%E8%BF%90%E8%A1%8C%E6%97%B6%E6%95%B0%E6%8D%AE%E5%8C%BA%E5%9F%9F.md)
1. [Minor GC和Full GC区别](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/Minor%20GC%E5%92%8CFull%20GC%E5%8C%BA%E5%88%AB.md)
1. [JVM垃圾回收机制：垃圾回收算法 垃圾回收器 垃圾回收策略](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/JVM%E5%9E%83%E5%9C%BE%E5%9B%9E%E6%94%B6%E6%9C%BA%E5%88%B6.md)
1. [jvm参数的设置和jvm调优](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/jvm%E5%8F%82%E6%95%B0%E7%9A%84%E8%AE%BE%E7%BD%AE%E5%92%8Cjvm%E8%B0%83%E4%BC%98.md)
1. 什么情况产生年轻代内存溢出、什么情况产生年老代内存溢出
1. [Java内存模型之happens-before](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/happens-before.md)
1. [jvm参数的设置](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/jvm%E5%8F%82%E6%95%B0%E7%9A%84%E8%AE%BE%E7%BD%AE%E5%92%8Cjvm%E8%B0%83%E4%BC%98.md)
1. [运行时数据区域](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/%E8%BF%90%E8%A1%8C%E6%97%B6%E6%95%B0%E6%8D%AE%E5%8C%BA%E5%9F%9F.md)
1. [字符串常量池、class常量池和运行时常量池](https://blog.csdn.net/qq_26222859/article/details/73135660)



#### 多线程并发
1. [Synchronized的实现原理](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/Synchronize%20%E5%85%B3%E9%94%AE%E5%AD%97%E5%8E%9F%E7%90%86.md)
1. [并发之CAS操作](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/%E5%B9%B6%E5%8F%91%E4%B9%8BCAS%E6%93%8D%E4%BD%9C.md)
1. [并发之AQS操作](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/AQS.md)
1. [多线程池](https://gitee.com/qinxuewu/basic_induction_of_java/edit/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E6%B1%A0.md)
1. [volatile关键字解析](https://gitee.com/qinxuewu/basic_induction_of_java/edit/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/volatile%E5%85%B3%E9%94%AE%E5%AD%97%E8%A7%A3%E6%9E%90.md)
1. [JMM内存模型（原子性，可见性，一致性，重排序）](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/JMM%E5%86%85%E5%AD%98%E6%A8%A1%E5%9E%8B.md)
1. [ConcurrentHashMap 实现原理](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/ConcurrentHashMap%20%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86.md)
1. [ReentrantLock 重入锁实现原理](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/ReentrantLock%20%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86.md)
1. [ReentrantReadWriteLock 读写锁实现原理](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/ReentrantReadWriteLock.md)
1. [乐观锁和悲观锁的实现](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/%E4%B9%90%E8%A7%82%E9%94%81%E5%92%8C%E6%82%B2%E8%A7%82%E9%94%81%E7%9A%84%E5%AE%9E%E7%8E%B0.md)
1. [死锁代码实现](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/%E6%AD%BB%E9%94%81.md)
1. Disruptor并发框架
1. [同步屏障CyclicBarrier](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/CyclicBarrier%20.md)
1. [等待多线程完成的CountDownLatch](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/CountDownLatch.md)
1. [控制并发线程数的Semaphore](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/Semaphore.md)
1. [Exchanger（交换者）是一个用于线程间协作的工具类](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/Exchanger.md)
1. [JUC之阻塞队列：SynchronousQueue](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/SynchronousQueue.md)
1. [JUC之阻塞队列：ArrayBlockingQueu](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B%E5%B9%B6%E5%8F%91/ArrayBlockingQueue.md#juc%E4%B9%8B%E9%98%BB%E5%A1%9E%E9%98%9F%E5%88%97arrayblockingqueu)

### 分布式相关
1. [分布式限流](https://github.com/crossoverJie/distributed-redis-tool)
1. [分布式缓存设计](https://github.com/crossoverJie/Java-Interview/blob/master/MD/Cache-design.md)
1. [分布式 ID 生成器](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/%E5%88%86%E5%B8%83%E5%BC%8FID%E7%94%9F%E6%88%90%E5%99%A8.md)
1. [基于 Redis 的分布式锁](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/%E5%9F%BA%E4%BA%8E%20Redis%20%E7%9A%84%E5%88%86%E5%B8%83%E5%BC%8F%E9%94%81.md)
1. [为什么分布式一定要有Redis？](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/%E4%B8%BA%E4%BB%80%E4%B9%88%E5%88%86%E5%B8%83%E5%BC%8F%E4%B8%80%E5%AE%9A%E8%A6%81%E6%9C%89Redis.md)
1. [基于Zookeeper实现分布式锁](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/Zookeeper.md)
1. zookeeper相关，节点类型，如何实现服务发现和服务注册
1. [nginx负载均衡实现](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/nginx.md)
1. ActiveMQ、RabbitMQ、Kafka的区别 
1. [HBase安装配置](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/Hbase.md)
1. [kafka安装配置与使用](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/Kafka%E5%AE%89%E8%A3%85%E4%B8%8E%E4%BD%BF%E7%94%A8.md)
1. [RocketMQ安装与使用](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/RocketMQ.md)
1. [ElasticSearch搜索引擎的安装与使用](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/Elasticsearch.md)
1. [消息队列的使用场景](https://blog.csdn.net/seven__________7/article/details/70225830)
1. [缓存和数据库一致性同步解决方案](https://blog.csdn.net/simba_1986/article/details/77823309#commentBox)
1. 阻塞、非阻塞、同步、异步区别
1. [什么是分布式事物](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%88%86%E5%B8%83%E5%BC%8F%E7%9B%B8%E5%85%B3/%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E7%89%A9.md)


#### 数据库相关
1. MySQL 索引原理
1. [数据库水平垂直拆分](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/sql%E4%BC%98%E5%8C%96/%E6%95%B0%E6%8D%AE%E5%BA%93%E6%B0%B4%E5%B9%B3%E5%9E%82%E7%9B%B4%E6%8B%86%E5%88%86.md)
1. [SQL 优化](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/sql%E4%BC%98%E5%8C%96/%E7%B4%A2%E5%BC%95.md)
1. [什么情况索引不会命中，会造成全表扫描](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/sql%E4%BC%98%E5%8C%96/%E5%85%A8%E8%A1%A8%E6%89%AB%E6%8F%8F.md)
1. mysql索引的实现 B+树的实现原理


#### http协议
1. [TCP/IP协议](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/http%E5%8D%8F%E8%AE%AE/TCP-IP%E5%8D%8F%E8%AE%AE.md)
1. http请求报文结构和内容



#### 设计模式
 **创建型** 
- [抽象工厂模式，工厂方法，建造者模式，原型模式，单例模式](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/%E5%88%9B%E5%BB%BA%E5%9E%8B%E6%A8%A1%E5%BC%8F.md)

 **结构型** 
- [适配器模式,桥接模式,组合模式,外观模式,装饰者模式,享元模式,代理模式](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/%E7%BB%93%E6%9E%84%E5%9E%8B.md)

 **行为型** 
- [责任链模式,命令模式,解释器模式,迭代模式,中介者模式,备忘录模式,观察者模式,状态模式](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/%E8%A1%8C%E4%B8%BA%E5%9E%8B%E6%A8%A1%E5%BC%8F.md)
- [策略模式,模板方法模式,访问者模式](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/%E8%A1%8C%E4%B8%BA%E5%9E%8B%E6%A8%A1%E5%BC%8F.md)

#### 数据结构与算法
- [红包算法](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/red/RedPacket.java)
- [二叉树中序遍历](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/BinaryNode.java#L76-L101)
- [是否为快乐数字](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/HappyNum.java#L38-L55)
- [链表是否有环](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/LinkLoop.java#L32-L59)
- [从一个数组中返回两个值相加等于目标值的下标](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/TwoSum.java#L38-L59)
- [一致性 Hash 算法](https://github.com/crossoverJie/Java-Interview/blob/master/MD/Consistent-Hash.md)
- [限流算法](https://github.com/crossoverJie/Java-Interview/blob/master/MD/Limiting.md)
- [三种方式反向打印单向链表](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/ReverseNode.java)
- [合并两个排好序的链表](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/MergeTwoSortedLists.java)
- [两个栈实现队列](https://github.com/crossoverJie/Java-Interview/blob/master/src/main/java/com/crossoverjie/algorithm/TwoStackQueue.java)
- [动手实现一个 LRU cache](http://crossoverjie.top/2018/04/07/algorithm/LRU-cache/)

#### spring/springBoot/springCloud相关
- [SpringMVC运行流程及九大组件
](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/spring%E7%9B%B8%E5%85%B3/springMvc%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86.md)
- [Spring AOP 的实现原理](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/spring%E7%9B%B8%E5%85%B3/aop%E7%9A%84%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86.md)
- [IOC容器初始化过程](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/spring%E7%9B%B8%E5%85%B3/ioc.md)
- [Spring Bean 生命周期](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/spring%E7%9B%B8%E5%85%B3/Spring%20Bean%20%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F.md)



### 其它
- [dubbo入门demo（zookeeper 安装）](https://gitee.com/qinxuewu/dubbo-demo)
- [Sublime Text常用插件](https://gitee.com/qinxuewu/BiJi/blob/master/Sublime%20Text%E5%B8%B8%E7%94%A8%E6%8F%92%E4%BB%B6.md)
- [Linxu服务命令](https://gitee.com/qinxuewu/BiJi/blob/master/Linxu%E6%9C%8D%E5%8A%A1%E5%91%BD%E4%BB%A4.md)
- [windows装（glup打包）](https://gitee.com/qinxuewu/BiJi/blob/master/README.md)
- [fastdfs文件服务器安装](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/fastdfs%E5%AE%89%E8%A3%85%E9%85%8D%E7%BD%AE.md)
- [Windows Docker 安装](https://blog.csdn.net/u010391342/article/details/80733171)
- [Dockerfile创建Java web项目镜像](https://blog.csdn.net/u010391342/article/details/80734590)
- [mysql怎样导入超过5G以上的sql文件](https://blog.csdn.net/u010391342/article/details/78767972)
- [Java操作 ActiveMQ 远程监控JMX设置](https://blog.csdn.net/u010391342/article/details/77837511)
- [ idea调试Spring源码.md](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/idea%E8%B0%83%E8%AF%95Spring%E6%BA%90%E7%A0%81.md)
- [jdk监控和故障处理工具](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/JVM/jvm%E7%9B%91%E6%B5%8B%E5%91%BD%E4%BB%A4.md)


### 面试题
1. [多线程系列](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/%E5%A4%9A%E7%BA%BF%E7%A8%8B.md)
1. [spring系列](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/spring.md)
1. [redis系列](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/redis%E9%9D%A2%E8%AF%95.md)
1. [jvm系列](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/jvm.md)
1. [Java基础系列](https://gitee.com/qinxuewu/basic_induction_of_java/blob/master/%E5%85%B6%E5%AE%83/%E9%9D%A2%E8%AF%95%E9%A2%98.md)


#### 文档
https://gitee.com/qinxuewu/basic_induction_of_java/wikis)


![输入图片说明](https://images.gitee.com/uploads/images/2018/1114/171732_0353c3b8_1478371.png "屏幕截图.png")

